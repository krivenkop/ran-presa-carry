<?php


namespace FrontendBundle\Controller;

use BackendBundle\Entity\Comment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommentController extends Controller
{
    public function storeAction(Request $request, EntityManagerInterface $em, $newsId)
    {
        $news = $em->getRepository('NewsBundle:News')->find($newsId);

        if (!$news) throw new NotFoundHttpException('Новость не найдена');

        $data = $request->get('store_comment');

        $comment = new Comment();

        $comment->setName($data["name"]);
        $comment->setContent($data["content"]);
        $comment->setShowOnWebsite(1);
        $comment->setPublishAt(null);
        $comment->setNews($news);

        $em->persist($comment);
        $em->flush();

        $referer = $_SERVER['HTTP_REFERER'] ?? $this->generateUrl('frontend_news_list');

        return new RedirectResponse($referer);
    }
}