<?php

namespace FrontendBundle\Controller;

use ComponentBundle\Utils\BreadcrumbsGenerator;
use NewsBundle\Entity\News;
use SeoBundle\Entity\SeoPage;
use StaticBundle\Entity\StaticPage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
final class FrontendController extends Controller
{
    private function helperForStaticPage(BreadcrumbsGenerator $breadcrumbsGenerator, EntityManagerInterface $em, $name, $routeName)
    {
        $static = $em->getRepository(StaticPage::class)->getStaticPageBySystemName($name);

        if (empty($static)) {
            return $this->redirectToRoute('frontend_homepage');
        }

        $breadcrumbsArr = [];
        $seoHomepage = $em->getRepository(SeoPage::class)->getSeoForPageBySystemName('homepage');
        $breadcrumbsArr['frontend_homepage'][] = [
            'parameters' => [],
            'title' => (!empty($seoHomepage) and !empty($seoHomepage->breadcrumb)) ? $seoHomepage->breadcrumb : ''
        ];

        $seo = $static->getSeo()->getSeoForPage();
        $breadcrumbsArr[$routeName][] = [
            'parameters' => [],
            'title' => (!empty($seo) and !empty($seo->breadcrumb)) ? $seo->breadcrumb : ''
        ];

        $breadcrumbs = $breadcrumbsGenerator->generateBreadcrumbs($breadcrumbsArr);

        return $this->render('frontend/'.$name.'/index.html.twig', [
            'seo' => $seo,
            'page' => $static,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    public function newsFeedAction(EntityManagerInterface $em){
        $newsFeed = $em->getRepository(News::class)->getLimitElements(10);
        return $this->render('frontend/news/_newsFeed.html.twig', [
            'newsFeed' => $newsFeed,
        ]);
    }
}