<?php

namespace FrontendBundle\Controller;

use Entity\Category;
use FrontendBundle\Entity\WpPostmeta;
use FrontendBundle\Entity\WpPosts;
use FrontendBundle\Entity\WpTermRelationships;
use FrontendBundle\Entity\WpTerms;
use FrontendBundle\Entity\WpTermTaxonomy;
use BackendBundle\Entity\Banner;
use BackendBundle\Entity\Info;
use BackendBundle\Entity\Opinion;
use BackendBundle\Entity\Video;
use NewsBundle\Entity\News;
use NewsBundle\Entity\NewsCategory;
use SeoBundle\Entity\Seo;
use SeoBundle\Entity\SeoPage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UploadBundle\Services\FileHandler;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
final class HomepageController extends Controller
{
//    public function indexAction(EntityManagerInterface $em)
//    {
//        $seo = $em->getRepository(SeoPage::class)->getSeoForPageBySystemName('homepage');
//        $newsSlider = $em->getRepository(News::class)->getLimitElements(3);
//        $newsRAND = $em->getRepository(News::class)->getLimitRANDElements(2);
//        $video = $em->getRepository(Video::class)->getLimitElements(2);
//        $banners = $em->getRepository(Banner::class)->getByPage('homepage');
//        $info = $em->getRepository(Info::class)->getLimitElements(2);
//        $opinion = $em->getRepository(Opinion::class)->getLimitElements(2);
//
//
//
//        return $this->render('frontend/homepage/index.html.twig', [
//            'seo' => $seo,
//            'newsSlider' => $newsSlider,
//            'newsRAND' => $newsRAND,
//            'video' => $video,
//            'banners' => $banners,
//            'info' => $info,
//            'opinion' => $opinion
//        ]);
//    }

    public function indexAction(Request $request, FileHandler $fileHandler)
    {
        $em = $this->getDoctrine()->getManager();

        $WpTermTaxonomy = $em->getRepository(WpTermTaxonomy::class)->findBy([
            'taxonomy' => 'category'
        ]);

        $newsCategories = [];
        foreach ($WpTermTaxonomy as $taxonomy) {
            $category = $em->getRepository(WpTerms::class)->findOneBy([
                'termId' => $taxonomy->getTermId()
            ]);
            if (!empty($category) and $category->getName() != 'Экипировка') {
                $newsCategories[$category->getName()] = [
                    'id' => $category->getTermId(),
                    'name' => $category->getName(),
                    'items' => []
                ];
            }
        }
        foreach ($newsCategories as $key => $category) {
            $WpTermRelationships = $em->getRepository(WpTermRelationships::class)->findBy([
                'termTaxonomyId' => $category['id']
            ]);
            foreach ($WpTermRelationships as $item) {
                $post = $em->getRepository(WpPosts::class)->findOneBy(['id' => $item->getObjectId()]);
                if (!empty($post)) {
                    $image = null;
                    $posts = $em->getRepository(WpPosts::class)->findBy([
//                        'metaKey' => '_wp_attached_file',
                        'postParent' => $post->getId()
                    ]);
                    if ($posts) {
                        foreach ($posts as $postsItem) {
                            $WpPostmeta = $em->getRepository(WpPostmeta::class)->findOneBy([
                                'metaKey' => '_wp_attached_file',
                                'postId' => $postsItem->getId()
                            ]);
                            if (!empty($WpPostmeta)) {
                                $image = $WpPostmeta->getMetaValue();
                                break;
                            }
                        }
                    }
                    $postArr = [
                        'postDate' => $post->getPostDate(),
                        'postContent' => $post->getPostContent(),
                        'postTitle' => $post->getPostTitle(),
                        'postExcerpt' => $post->getPostExcerpt(),
                    ];
                    $category['items'][] = [
                        'image' => $image,
                        'post' => $postArr
                    ];
                }
            }
            $newsCategories[$key] = $category;
        }

        $newsRepo = $em->getRepository(NewsCategory::class);

        $counter = 1;
        foreach ($newsCategories as $category) {
            $cat = $newsRepo->createQueryBuilder('q')
                ->select('q, t, seo, seo_t')
                ->leftJoin('q.translations', 't')
                ->leftJoin('q.seo', 'seo')
                ->leftJoin('seo.translations', 'seo_t')
                ->where('t.title = :name')
                ->setParameters(['name' => $category['name']])
                ->getQuery()
                ->getFirstResult();

            if (!$cat) {
                $cat = new NewsCategory();

                $seoNews = new Seo();
                $seoNews->translate('ru')->setMetaTitle($category['name']);
                $seoNews->translate('ru')->setH1($category['name']);
                $seoNews->translate('ru')->setBreadcrumb($category['name']);
                $seoNews->mergeNewTranslations();
                $em->persist($seoNews);

                $cat->setSeo($seoNews);
                $cat->setShowOnWebsite(1);
                $cat->setPosition($counter);
                $cat->setCurrentLocale('ru');
                $cat->setDefaultLocale('ru');
                $cat->translate()->setTitle($category['name']);
                $cat->mergeNewTranslations();

                $em->persist($cat);
                $em->flush();
            }

            foreach ($category['items'] as $item) {
                if (!$item['post']['postTitle']) continue;

                $news = $em->getRepository('NewsBundle:News')
                    ->createQueryBuilder('q')
                    ->select('q, t, seo, seo_t')
                    ->leftJoin('q.translations', 't')
                    ->leftJoin('q.seo', 'seo')
                    ->leftJoin('seo.translations', 'seo_t')
                    ->where('t.title = :title')
                    ->setParameters(['title' => $item['post']['postTitle']])
                    ->getQuery()
                    ->getFirstResult();

                if (!$news) {
                    $news = new News();
                    $seo = new Seo();
                    $seo->translate('ru')->setMetaTitle($item['post']['postTitle']);
                    $seo->translate('ru')->setH1($item['post']['postTitle']);
                    $seo->translate('ru')->setBreadcrumb($item['post']['postTitle']);
                    $seo->mergeNewTranslations();
                    $em->persist($seo);
                    $news->setSeo($seo);
                    $news->setNewsCategory($cat);
                    $news->translate('ru')->setTitle($item['post']['postTitle']);
                    $news->translate('ru')->setDescription($item['post']['postContent']);
                    $news->translate('ru')->setShortDescription($item['post']['postExcerpt']);
                    $news->setPublishAt($item['post']['postDate']->format('Y-m-d H:i:s'));
                    $news->mergeNewTranslations();
                    $em->persist($news);
                    $em->flush();
                }

                try {
                    if (!empty($item['image'])) {
                        $subDir = '/' . $news->getId();
                        $filePath['file_type'] = 'news_poster';
                        $filePath['field'] = 'image';
                        $filePath['path'] = 'http://ranpress.ru/wp-content/uploads/' . $item['image'];
                        $resultRes = $fileHandler->handleFileAndSave($filePath, $subDir, false);
                        if (!empty($resultRes)) {
                            $news->setPoster(json_encode($resultRes));
                            $em->persist($news);
                            $em->flush();
                        }
                    }
                } catch (\Exception $e) {
                }
                $counter += 1;
            }
        }
        return new Response('ok');
    }

}