<?php

namespace FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
final class SiteMapController extends AbstractController
{
    public function siteMapAction(Request $request, EntityManagerInterface $em)
    {
        $hostname = $request->getSchemeAndHttpHost();
        $router = $this->get('router');
        $urls = [];
        $urls[] = [
            'loc' => $router->generate('frontend_homepage'),
            'changefreq' => 'weekly',
            'priority' => '1.0'
        ];

        return $this->render('@Component/_site_map.xml.twig', [
            'urls' => $urls, 'hostname' => $hostname
        ]);
    }
}