<?php

namespace FrontendBundle\Controller;

use NewsBundle\Entity\News;
use StaticBundle\Entity\StaticContent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use BackendBundle\Entity\Banner;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
final class DefaultController extends Controller
{
    public function getStaticContentAction(EntityManagerInterface $em, $page)
    {
        $static = $em->getRepository(StaticContent::class)->getByPage($page);

        $result = [];
        foreach ($static as $item) {
            $result[$item->getLinkName()]['text'] = $item->translate()->getText();
            $result[$item->getLinkName()]['poster'] = $item->getImg();
        }

        return new JsonResponse($result);
    }

    public function initHeaderAction(Request $request)
    {
        return $this->render('frontend/default/_header.html.twig', [
            'request' => $request,
        ]);
    }

    public function initFooterAction(EntityManagerInterface $em, Request $request)
    {
        $static = $em->getRepository(StaticContent::class)->getByPage('footer');
        return $this->render('frontend/default/_footer.html.twig', [
            'request' => $request,
            'static' => $static,
        ]);
    }

    public function initSidebarBannerAction(EntityManagerInterface $em)
    {
        $banners = $em->getRepository(Banner::class)->getByPage('homepage');
        return $this->render('frontend/default/_sidebar_banner.html.twig', [
            'banners' => $banners,
        ]);
    }

    public function initSidebarNewsAction(EntityManagerInterface $em)
    {
        $news = $em->getRepository(News::class)->getLimitElements(10);
        return $this->render('frontend/news/_sidebar_news.html.twig', [
            'news' => $news,
        ]);
    }

    public function initNewsCategorySliderAction(EntityManagerInterface $em, $slug)
    {
        if ($slug == '') {
            $news = $em->getRepository(News::class)->getElementsForList(3);
        } else {
            $news = $em->getRepository(News::class)->getElementsByCategoryLimit($slug, 3);
        }
        return $this->render('frontend/news_category/_slider.html.twig', [
            'news' => $news,
        ]);
    }

    public function initSingleNewsSliderAction(EntityManagerInterface $em) {
        $newsSlider = $em->getRepository(News::class)->getLimitElements(3);

        return $this->render('frontend/homepage/_singleNewsSlider.html.twig', [
            'newsSlider' => $newsSlider
        ]);
    }
}