<?php

namespace FrontendBundle\Controller;

use BackendBundle\Entity\Video;
use ComponentBundle\Utils\BreadcrumbsGenerator;
use SeoBundle\Entity\SeoPage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
final class VideoController extends Controller
{
    private function helperForBreadcrumbsInIndexAction(BreadcrumbsGenerator $breadcrumbsGenerator, EntityManagerInterface $em, $seo)
    {
        $breadcrumbsArr = [];
        $seoHomepage = $em->getRepository(SeoPage::class)->getSeoForPageBySystemName('homepage');
        $breadcrumbsArr['frontend_homepage'][] = [
            'parameters' => [],
            'title' => (!empty($seoHomepage) and !empty($seoHomepage->breadcrumb)) ? $seoHomepage->breadcrumb : ''
        ];

        $breadcrumbsArr['frontend_video_list'][] = [
            'parameters' => [],
            'title' => (!empty($seo) and !empty($seo->breadcrumb)) ? $seo->breadcrumb : ''
        ];

        return $breadcrumbsGenerator->generateBreadcrumbs($breadcrumbsArr);
    }


    public function indexAction(Request $request, BreadcrumbsGenerator $breadcrumbsGenerator, EntityManagerInterface $em, $page = null, $countInPage = null)
    {
        $seo = $em->getRepository(SeoPage::class)->getSeoForPageBySystemName('video');

        $breadcrumbs = self::helperForBreadcrumbsInIndexAction($breadcrumbsGenerator, $em, $seo);
        $videoRepository = $em->getRepository(Video::class);
        $videos = $videoRepository->getLimitElements(null);

        return $this->render('frontend/videos/index.html.twig', [
            'seo' => $seo,
            'breadcrumbs' => $breadcrumbs,
            'videos' => $videos
        ]);
    }
}