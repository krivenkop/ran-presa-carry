<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpWysijaUserField
 *
 * @ORM\Table(name="wp_wysija_user_field")
 * @ORM\Entity
 */
class WpWysijaUserField
{
    /**
     * @var integer
     *
     * @ORM\Column(name="field_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $fieldId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=250, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="column_name", type="string", length=250, nullable=false)
     */
    private $columnName = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean", nullable=true)
     */
    private $type = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="values", type="text", length=65535, nullable=true)
     */
    private $values;

    /**
     * @var string
     *
     * @ORM\Column(name="default", type="string", length=250, nullable=false)
     */
    private $default = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_required", type="boolean", nullable=false)
     */
    private $isRequired = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="error_message", type="string", length=250, nullable=false)
     */
    private $errorMessage = '';



    /**
     * Get fieldId
     *
     * @return integer
     */
    public function getFieldId()
    {
        return $this->fieldId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return WpWysijaUserField
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set columnName
     *
     * @param string $columnName
     *
     * @return WpWysijaUserField
     */
    public function setColumnName($columnName)
    {
        $this->columnName = $columnName;

        return $this;
    }

    /**
     * Get columnName
     *
     * @return string
     */
    public function getColumnName()
    {
        return $this->columnName;
    }

    /**
     * Set type
     *
     * @param boolean $type
     *
     * @return WpWysijaUserField
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return boolean
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set values
     *
     * @param string $values
     *
     * @return WpWysijaUserField
     */
    public function setValues($values)
    {
        $this->values = $values;

        return $this;
    }

    /**
     * Get values
     *
     * @return string
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Set default
     *
     * @param string $default
     *
     * @return WpWysijaUserField
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default
     *
     * @return string
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set isRequired
     *
     * @param boolean $isRequired
     *
     * @return WpWysijaUserField
     */
    public function setIsRequired($isRequired)
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    /**
     * Get isRequired
     *
     * @return boolean
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Set errorMessage
     *
     * @param string $errorMessage
     *
     * @return WpWysijaUserField
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    /**
     * Get errorMessage
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
}
