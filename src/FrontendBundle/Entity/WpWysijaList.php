<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpWysijaList
 *
 * @ORM\Table(name="wp_wysija_list")
 * @ORM\Entity
 */
class WpWysijaList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="list_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $listId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=250, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="namekey", type="string", length=255, nullable=true)
     */
    private $namekey;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="unsub_mail_id", type="integer", nullable=false)
     */
    private $unsubMailId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="welcome_mail_id", type="integer", nullable=false)
     */
    private $welcomeMailId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_enabled", type="boolean", nullable=false)
     */
    private $isEnabled = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_public", type="boolean", nullable=false)
     */
    private $isPublic = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="integer", nullable=true)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordering", type="integer", nullable=false)
     */
    private $ordering = '0';



    /**
     * Get listId
     *
     * @return integer
     */
    public function getListId()
    {
        return $this->listId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return WpWysijaList
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set namekey
     *
     * @param string $namekey
     *
     * @return WpWysijaList
     */
    public function setNamekey($namekey)
    {
        $this->namekey = $namekey;

        return $this;
    }

    /**
     * Get namekey
     *
     * @return string
     */
    public function getNamekey()
    {
        return $this->namekey;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return WpWysijaList
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set unsubMailId
     *
     * @param integer $unsubMailId
     *
     * @return WpWysijaList
     */
    public function setUnsubMailId($unsubMailId)
    {
        $this->unsubMailId = $unsubMailId;

        return $this;
    }

    /**
     * Get unsubMailId
     *
     * @return integer
     */
    public function getUnsubMailId()
    {
        return $this->unsubMailId;
    }

    /**
     * Set welcomeMailId
     *
     * @param integer $welcomeMailId
     *
     * @return WpWysijaList
     */
    public function setWelcomeMailId($welcomeMailId)
    {
        $this->welcomeMailId = $welcomeMailId;

        return $this;
    }

    /**
     * Get welcomeMailId
     *
     * @return integer
     */
    public function getWelcomeMailId()
    {
        return $this->welcomeMailId;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return WpWysijaList
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set isPublic
     *
     * @param boolean $isPublic
     *
     * @return WpWysijaList
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic
     *
     * @return boolean
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set createdAt
     *
     * @param integer $createdAt
     *
     * @return WpWysijaList
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return integer
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set ordering
     *
     * @param integer $ordering
     *
     * @return WpWysijaList
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return integer
     */
    public function getOrdering()
    {
        return $this->ordering;
    }
}
