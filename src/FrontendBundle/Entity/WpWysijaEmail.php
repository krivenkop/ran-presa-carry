<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpWysijaEmail
 *
 * @ORM\Table(name="wp_wysija_email")
 * @ORM\Entity
 */
class WpWysijaEmail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="email_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $emailId;

    /**
     * @var integer
     *
     * @ORM\Column(name="campaign_id", type="integer", nullable=false)
     */
    private $campaignId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=250, nullable=false)
     */
    private $subject = '';

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="integer", nullable=true)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified_at", type="integer", nullable=true)
     */
    private $modifiedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="sent_at", type="integer", nullable=true)
     */
    private $sentAt;

    /**
     * @var string
     *
     * @ORM\Column(name="from_email", type="string", length=250, nullable=true)
     */
    private $fromEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="from_name", type="string", length=250, nullable=true)
     */
    private $fromName;

    /**
     * @var string
     *
     * @ORM\Column(name="replyto_email", type="string", length=250, nullable=true)
     */
    private $replytoEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="replyto_name", type="string", length=250, nullable=true)
     */
    private $replytoName;

    /**
     * @var string
     *
     * @ORM\Column(name="attachments", type="text", length=65535, nullable=true)
     */
    private $attachments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean", nullable=false)
     */
    private $type = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="number_sent", type="integer", nullable=false)
     */
    private $numberSent = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="number_opened", type="integer", nullable=false)
     */
    private $numberOpened = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="number_clicked", type="integer", nullable=false)
     */
    private $numberClicked = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="number_unsub", type="integer", nullable=false)
     */
    private $numberUnsub = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="number_bounce", type="integer", nullable=false)
     */
    private $numberBounce = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="number_forward", type="integer", nullable=false)
     */
    private $numberForward = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="text", length=65535, nullable=true)
     */
    private $params;

    /**
     * @var string
     *
     * @ORM\Column(name="wj_data", type="text", nullable=true)
     */
    private $wjData;

    /**
     * @var string
     *
     * @ORM\Column(name="wj_styles", type="text", nullable=true)
     */
    private $wjStyles;



    /**
     * Get emailId
     *
     * @return integer
     */
    public function getEmailId()
    {
        return $this->emailId;
    }

    /**
     * Set campaignId
     *
     * @param integer $campaignId
     *
     * @return WpWysijaEmail
     */
    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;

        return $this;
    }

    /**
     * Get campaignId
     *
     * @return integer
     */
    public function getCampaignId()
    {
        return $this->campaignId;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return WpWysijaEmail
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return WpWysijaEmail
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set createdAt
     *
     * @param integer $createdAt
     *
     * @return WpWysijaEmail
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return integer
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param integer $modifiedAt
     *
     * @return WpWysijaEmail
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return integer
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set sentAt
     *
     * @param integer $sentAt
     *
     * @return WpWysijaEmail
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * Get sentAt
     *
     * @return integer
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * Set fromEmail
     *
     * @param string $fromEmail
     *
     * @return WpWysijaEmail
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    /**
     * Get fromEmail
     *
     * @return string
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * Set fromName
     *
     * @param string $fromName
     *
     * @return WpWysijaEmail
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;

        return $this;
    }

    /**
     * Get fromName
     *
     * @return string
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * Set replytoEmail
     *
     * @param string $replytoEmail
     *
     * @return WpWysijaEmail
     */
    public function setReplytoEmail($replytoEmail)
    {
        $this->replytoEmail = $replytoEmail;

        return $this;
    }

    /**
     * Get replytoEmail
     *
     * @return string
     */
    public function getReplytoEmail()
    {
        return $this->replytoEmail;
    }

    /**
     * Set replytoName
     *
     * @param string $replytoName
     *
     * @return WpWysijaEmail
     */
    public function setReplytoName($replytoName)
    {
        $this->replytoName = $replytoName;

        return $this;
    }

    /**
     * Get replytoName
     *
     * @return string
     */
    public function getReplytoName()
    {
        return $this->replytoName;
    }

    /**
     * Set attachments
     *
     * @param string $attachments
     *
     * @return WpWysijaEmail
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments
     *
     * @return string
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return WpWysijaEmail
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type
     *
     * @param boolean $type
     *
     * @return WpWysijaEmail
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return boolean
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set numberSent
     *
     * @param integer $numberSent
     *
     * @return WpWysijaEmail
     */
    public function setNumberSent($numberSent)
    {
        $this->numberSent = $numberSent;

        return $this;
    }

    /**
     * Get numberSent
     *
     * @return integer
     */
    public function getNumberSent()
    {
        return $this->numberSent;
    }

    /**
     * Set numberOpened
     *
     * @param integer $numberOpened
     *
     * @return WpWysijaEmail
     */
    public function setNumberOpened($numberOpened)
    {
        $this->numberOpened = $numberOpened;

        return $this;
    }

    /**
     * Get numberOpened
     *
     * @return integer
     */
    public function getNumberOpened()
    {
        return $this->numberOpened;
    }

    /**
     * Set numberClicked
     *
     * @param integer $numberClicked
     *
     * @return WpWysijaEmail
     */
    public function setNumberClicked($numberClicked)
    {
        $this->numberClicked = $numberClicked;

        return $this;
    }

    /**
     * Get numberClicked
     *
     * @return integer
     */
    public function getNumberClicked()
    {
        return $this->numberClicked;
    }

    /**
     * Set numberUnsub
     *
     * @param integer $numberUnsub
     *
     * @return WpWysijaEmail
     */
    public function setNumberUnsub($numberUnsub)
    {
        $this->numberUnsub = $numberUnsub;

        return $this;
    }

    /**
     * Get numberUnsub
     *
     * @return integer
     */
    public function getNumberUnsub()
    {
        return $this->numberUnsub;
    }

    /**
     * Set numberBounce
     *
     * @param integer $numberBounce
     *
     * @return WpWysijaEmail
     */
    public function setNumberBounce($numberBounce)
    {
        $this->numberBounce = $numberBounce;

        return $this;
    }

    /**
     * Get numberBounce
     *
     * @return integer
     */
    public function getNumberBounce()
    {
        return $this->numberBounce;
    }

    /**
     * Set numberForward
     *
     * @param integer $numberForward
     *
     * @return WpWysijaEmail
     */
    public function setNumberForward($numberForward)
    {
        $this->numberForward = $numberForward;

        return $this;
    }

    /**
     * Get numberForward
     *
     * @return integer
     */
    public function getNumberForward()
    {
        return $this->numberForward;
    }

    /**
     * Set params
     *
     * @param string $params
     *
     * @return WpWysijaEmail
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Get params
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set wjData
     *
     * @param string $wjData
     *
     * @return WpWysijaEmail
     */
    public function setWjData($wjData)
    {
        $this->wjData = $wjData;

        return $this;
    }

    /**
     * Get wjData
     *
     * @return string
     */
    public function getWjData()
    {
        return $this->wjData;
    }

    /**
     * Set wjStyles
     *
     * @param string $wjStyles
     *
     * @return WpWysijaEmail
     */
    public function setWjStyles($wjStyles)
    {
        $this->wjStyles = $wjStyles;

        return $this;
    }

    /**
     * Get wjStyles
     *
     * @return string
     */
    public function getWjStyles()
    {
        return $this->wjStyles;
    }
}
