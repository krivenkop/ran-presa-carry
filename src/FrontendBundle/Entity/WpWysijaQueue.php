<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpWysijaQueue
 *
 * @ORM\Table(name="wp_wysija_queue", indexes={@ORM\Index(name="SENT_AT_INDEX", columns={"send_at"})})
 * @ORM\Entity
 */
class WpWysijaQueue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="email_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $emailId;

    /**
     * @var integer
     *
     * @ORM\Column(name="send_at", type="integer", nullable=false)
     */
    private $sendAt = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="priority", type="boolean", nullable=false)
     */
    private $priority = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="number_try", type="boolean", nullable=false)
     */
    private $numberTry = '0';



    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return WpWysijaQueue
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set emailId
     *
     * @param integer $emailId
     *
     * @return WpWysijaQueue
     */
    public function setEmailId($emailId)
    {
        $this->emailId = $emailId;

        return $this;
    }

    /**
     * Get emailId
     *
     * @return integer
     */
    public function getEmailId()
    {
        return $this->emailId;
    }

    /**
     * Set sendAt
     *
     * @param integer $sendAt
     *
     * @return WpWysijaQueue
     */
    public function setSendAt($sendAt)
    {
        $this->sendAt = $sendAt;

        return $this;
    }

    /**
     * Get sendAt
     *
     * @return integer
     */
    public function getSendAt()
    {
        return $this->sendAt;
    }

    /**
     * Set priority
     *
     * @param boolean $priority
     *
     * @return WpWysijaQueue
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return boolean
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set numberTry
     *
     * @param boolean $numberTry
     *
     * @return WpWysijaQueue
     */
    public function setNumberTry($numberTry)
    {
        $this->numberTry = $numberTry;

        return $this;
    }

    /**
     * Get numberTry
     *
     * @return boolean
     */
    public function getNumberTry()
    {
        return $this->numberTry;
    }
}
