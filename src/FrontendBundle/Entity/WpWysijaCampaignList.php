<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpWysijaCampaignList
 *
 * @ORM\Table(name="wp_wysija_campaign_list")
 * @ORM\Entity
 */
class WpWysijaCampaignList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="list_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $listId;

    /**
     * @var integer
     *
     * @ORM\Column(name="campaign_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $campaignId;

    /**
     * @var string
     *
     * @ORM\Column(name="filter", type="text", length=65535, nullable=true)
     */
    private $filter;



    /**
     * Set listId
     *
     * @param integer $listId
     *
     * @return WpWysijaCampaignList
     */
    public function setListId($listId)
    {
        $this->listId = $listId;

        return $this;
    }

    /**
     * Get listId
     *
     * @return integer
     */
    public function getListId()
    {
        return $this->listId;
    }

    /**
     * Set campaignId
     *
     * @param integer $campaignId
     *
     * @return WpWysijaCampaignList
     */
    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;

        return $this;
    }

    /**
     * Get campaignId
     *
     * @return integer
     */
    public function getCampaignId()
    {
        return $this->campaignId;
    }

    /**
     * Set filter
     *
     * @param string $filter
     *
     * @return WpWysijaCampaignList
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * Get filter
     *
     * @return string
     */
    public function getFilter()
    {
        return $this->filter;
    }
}
