<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpDemocracyLog
 *
 * @ORM\Table(name="wp_democracy_log", indexes={@ORM\Index(name="ip", columns={"ip", "qid"}), @ORM\Index(name="qid", columns={"qid"}), @ORM\Index(name="userid", columns={"userid"})})
 * @ORM\Entity
 */
class WpDemocracyLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="logid", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $logid;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=100, nullable=false)
     */
    private $ip = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="qid", type="bigint", nullable=false)
     */
    private $qid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="aids", type="text", length=65535, nullable=false)
     */
    private $aids;

    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="bigint", nullable=false)
     */
    private $userid = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="expire", type="bigint", nullable=false)
     */
    private $expire = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ip_info", type="text", length=65535, nullable=false)
     */
    private $ipInfo;



    /**
     * Get logid
     *
     * @return integer
     */
    public function getLogid()
    {
        return $this->logid;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return WpDemocracyLog
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set qid
     *
     * @param integer $qid
     *
     * @return WpDemocracyLog
     */
    public function setQid($qid)
    {
        $this->qid = $qid;

        return $this;
    }

    /**
     * Get qid
     *
     * @return integer
     */
    public function getQid()
    {
        return $this->qid;
    }

    /**
     * Set aids
     *
     * @param string $aids
     *
     * @return WpDemocracyLog
     */
    public function setAids($aids)
    {
        $this->aids = $aids;

        return $this;
    }

    /**
     * Get aids
     *
     * @return string
     */
    public function getAids()
    {
        return $this->aids;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return WpDemocracyLog
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return WpDemocracyLog
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set expire
     *
     * @param integer $expire
     *
     * @return WpDemocracyLog
     */
    public function setExpire($expire)
    {
        $this->expire = $expire;

        return $this;
    }

    /**
     * Get expire
     *
     * @return integer
     */
    public function getExpire()
    {
        return $this->expire;
    }

    /**
     * Set ipInfo
     *
     * @param string $ipInfo
     *
     * @return WpDemocracyLog
     */
    public function setIpInfo($ipInfo)
    {
        $this->ipInfo = $ipInfo;

        return $this;
    }

    /**
     * Get ipInfo
     *
     * @return string
     */
    public function getIpInfo()
    {
        return $this->ipInfo;
    }
}
