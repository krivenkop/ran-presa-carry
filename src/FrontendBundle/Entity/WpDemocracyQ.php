<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpDemocracyQ
 *
 * @ORM\Table(name="wp_democracy_q", indexes={@ORM\Index(name="active", columns={"active"})})
 * @ORM\Entity
 */
class WpDemocracyQ
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="text", length=65535, nullable=false)
     */
    private $question;

    /**
     * @var integer
     *
     * @ORM\Column(name="added", type="integer", nullable=false)
     */
    private $added = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="added_user", type="bigint", nullable=false)
     */
    private $addedUser = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="end", type="integer", nullable=false)
     */
    private $end = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="users_voted", type="bigint", nullable=false)
     */
    private $usersVoted = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="democratic", type="boolean", nullable=false)
     */
    private $democratic = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="open", type="boolean", nullable=false)
     */
    private $open = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="multiple", type="boolean", nullable=false)
     */
    private $multiple = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="forusers", type="boolean", nullable=false)
     */
    private $forusers = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="revote", type="boolean", nullable=false)
     */
    private $revote = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_results", type="boolean", nullable=false)
     */
    private $showResults = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="answers_order", type="string", length=50, nullable=false)
     */
    private $answersOrder = '';

    /**
     * @var string
     *
     * @ORM\Column(name="in_posts", type="text", length=65535, nullable=false)
     */
    private $inPosts;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", length=65535, nullable=false)
     */
    private $note;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return WpDemocracyQ
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set added
     *
     * @param integer $added
     *
     * @return WpDemocracyQ
     */
    public function setAdded($added)
    {
        $this->added = $added;

        return $this;
    }

    /**
     * Get added
     *
     * @return integer
     */
    public function getAdded()
    {
        return $this->added;
    }

    /**
     * Set addedUser
     *
     * @param integer $addedUser
     *
     * @return WpDemocracyQ
     */
    public function setAddedUser($addedUser)
    {
        $this->addedUser = $addedUser;

        return $this;
    }

    /**
     * Get addedUser
     *
     * @return integer
     */
    public function getAddedUser()
    {
        return $this->addedUser;
    }

    /**
     * Set end
     *
     * @param integer $end
     *
     * @return WpDemocracyQ
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return integer
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set usersVoted
     *
     * @param integer $usersVoted
     *
     * @return WpDemocracyQ
     */
    public function setUsersVoted($usersVoted)
    {
        $this->usersVoted = $usersVoted;

        return $this;
    }

    /**
     * Get usersVoted
     *
     * @return integer
     */
    public function getUsersVoted()
    {
        return $this->usersVoted;
    }

    /**
     * Set democratic
     *
     * @param boolean $democratic
     *
     * @return WpDemocracyQ
     */
    public function setDemocratic($democratic)
    {
        $this->democratic = $democratic;

        return $this;
    }

    /**
     * Get democratic
     *
     * @return boolean
     */
    public function getDemocratic()
    {
        return $this->democratic;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return WpDemocracyQ
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set open
     *
     * @param boolean $open
     *
     * @return WpDemocracyQ
     */
    public function setOpen($open)
    {
        $this->open = $open;

        return $this;
    }

    /**
     * Get open
     *
     * @return boolean
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * Set multiple
     *
     * @param boolean $multiple
     *
     * @return WpDemocracyQ
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * Get multiple
     *
     * @return boolean
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

    /**
     * Set forusers
     *
     * @param boolean $forusers
     *
     * @return WpDemocracyQ
     */
    public function setForusers($forusers)
    {
        $this->forusers = $forusers;

        return $this;
    }

    /**
     * Get forusers
     *
     * @return boolean
     */
    public function getForusers()
    {
        return $this->forusers;
    }

    /**
     * Set revote
     *
     * @param boolean $revote
     *
     * @return WpDemocracyQ
     */
    public function setRevote($revote)
    {
        $this->revote = $revote;

        return $this;
    }

    /**
     * Get revote
     *
     * @return boolean
     */
    public function getRevote()
    {
        return $this->revote;
    }

    /**
     * Set showResults
     *
     * @param boolean $showResults
     *
     * @return WpDemocracyQ
     */
    public function setShowResults($showResults)
    {
        $this->showResults = $showResults;

        return $this;
    }

    /**
     * Get showResults
     *
     * @return boolean
     */
    public function getShowResults()
    {
        return $this->showResults;
    }

    /**
     * Set answersOrder
     *
     * @param string $answersOrder
     *
     * @return WpDemocracyQ
     */
    public function setAnswersOrder($answersOrder)
    {
        $this->answersOrder = $answersOrder;

        return $this;
    }

    /**
     * Get answersOrder
     *
     * @return string
     */
    public function getAnswersOrder()
    {
        return $this->answersOrder;
    }

    /**
     * Set inPosts
     *
     * @param string $inPosts
     *
     * @return WpDemocracyQ
     */
    public function setInPosts($inPosts)
    {
        $this->inPosts = $inPosts;

        return $this;
    }

    /**
     * Get inPosts
     *
     * @return string
     */
    public function getInPosts()
    {
        return $this->inPosts;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return WpDemocracyQ
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }
}
