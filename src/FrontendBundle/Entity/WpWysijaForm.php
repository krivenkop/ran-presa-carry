<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpWysijaForm
 *
 * @ORM\Table(name="wp_wysija_form")
 * @ORM\Entity
 */
class WpWysijaForm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="form_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $formId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="styles", type="text", nullable=true)
     */
    private $styles;

    /**
     * @var integer
     *
     * @ORM\Column(name="subscribed", type="integer", nullable=false)
     */
    private $subscribed = '0';



    /**
     * Get formId
     *
     * @return integer
     */
    public function getFormId()
    {
        return $this->formId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return WpWysijaForm
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return WpWysijaForm
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set styles
     *
     * @param string $styles
     *
     * @return WpWysijaForm
     */
    public function setStyles($styles)
    {
        $this->styles = $styles;

        return $this;
    }

    /**
     * Get styles
     *
     * @return string
     */
    public function getStyles()
    {
        return $this->styles;
    }

    /**
     * Set subscribed
     *
     * @param integer $subscribed
     *
     * @return WpWysijaForm
     */
    public function setSubscribed($subscribed)
    {
        $this->subscribed = $subscribed;

        return $this;
    }

    /**
     * Get subscribed
     *
     * @return integer
     */
    public function getSubscribed()
    {
        return $this->subscribed;
    }
}
