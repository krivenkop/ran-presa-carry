<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpWysijaUrlMail
 *
 * @ORM\Table(name="wp_wysija_url_mail")
 * @ORM\Entity
 */
class WpWysijaUrlMail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="email_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $emailId;

    /**
     * @var integer
     *
     * @ORM\Column(name="url_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $urlId;

    /**
     * @var integer
     *
     * @ORM\Column(name="unique_clicked", type="integer", nullable=false)
     */
    private $uniqueClicked = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="total_clicked", type="integer", nullable=false)
     */
    private $totalClicked = '0';



    /**
     * Set emailId
     *
     * @param integer $emailId
     *
     * @return WpWysijaUrlMail
     */
    public function setEmailId($emailId)
    {
        $this->emailId = $emailId;

        return $this;
    }

    /**
     * Get emailId
     *
     * @return integer
     */
    public function getEmailId()
    {
        return $this->emailId;
    }

    /**
     * Set urlId
     *
     * @param integer $urlId
     *
     * @return WpWysijaUrlMail
     */
    public function setUrlId($urlId)
    {
        $this->urlId = $urlId;

        return $this;
    }

    /**
     * Get urlId
     *
     * @return integer
     */
    public function getUrlId()
    {
        return $this->urlId;
    }

    /**
     * Set uniqueClicked
     *
     * @param integer $uniqueClicked
     *
     * @return WpWysijaUrlMail
     */
    public function setUniqueClicked($uniqueClicked)
    {
        $this->uniqueClicked = $uniqueClicked;

        return $this;
    }

    /**
     * Get uniqueClicked
     *
     * @return integer
     */
    public function getUniqueClicked()
    {
        return $this->uniqueClicked;
    }

    /**
     * Set totalClicked
     *
     * @param integer $totalClicked
     *
     * @return WpWysijaUrlMail
     */
    public function setTotalClicked($totalClicked)
    {
        $this->totalClicked = $totalClicked;

        return $this;
    }

    /**
     * Get totalClicked
     *
     * @return integer
     */
    public function getTotalClicked()
    {
        return $this->totalClicked;
    }
}
