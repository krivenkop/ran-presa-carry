<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpWysijaEmailUserUrl
 *
 * @ORM\Table(name="wp_wysija_email_user_url")
 * @ORM\Entity
 */
class WpWysijaEmailUserUrl
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="email_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $emailId;

    /**
     * @var integer
     *
     * @ORM\Column(name="url_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $urlId;

    /**
     * @var integer
     *
     * @ORM\Column(name="clicked_at", type="integer", nullable=true)
     */
    private $clickedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_clicked", type="integer", nullable=false)
     */
    private $numberClicked = '0';



    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return WpWysijaEmailUserUrl
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set emailId
     *
     * @param integer $emailId
     *
     * @return WpWysijaEmailUserUrl
     */
    public function setEmailId($emailId)
    {
        $this->emailId = $emailId;

        return $this;
    }

    /**
     * Get emailId
     *
     * @return integer
     */
    public function getEmailId()
    {
        return $this->emailId;
    }

    /**
     * Set urlId
     *
     * @param integer $urlId
     *
     * @return WpWysijaEmailUserUrl
     */
    public function setUrlId($urlId)
    {
        $this->urlId = $urlId;

        return $this;
    }

    /**
     * Get urlId
     *
     * @return integer
     */
    public function getUrlId()
    {
        return $this->urlId;
    }

    /**
     * Set clickedAt
     *
     * @param integer $clickedAt
     *
     * @return WpWysijaEmailUserUrl
     */
    public function setClickedAt($clickedAt)
    {
        $this->clickedAt = $clickedAt;

        return $this;
    }

    /**
     * Get clickedAt
     *
     * @return integer
     */
    public function getClickedAt()
    {
        return $this->clickedAt;
    }

    /**
     * Set numberClicked
     *
     * @param integer $numberClicked
     *
     * @return WpWysijaEmailUserUrl
     */
    public function setNumberClicked($numberClicked)
    {
        $this->numberClicked = $numberClicked;

        return $this;
    }

    /**
     * Get numberClicked
     *
     * @return integer
     */
    public function getNumberClicked()
    {
        return $this->numberClicked;
    }
}
