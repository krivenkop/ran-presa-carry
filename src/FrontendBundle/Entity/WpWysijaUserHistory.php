<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpWysijaUserHistory
 *
 * @ORM\Table(name="wp_wysija_user_history")
 * @ORM\Entity
 */
class WpWysijaUserHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="history_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $historyId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="email_id", type="integer", nullable=true)
     */
    private $emailId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=250, nullable=false)
     */
    private $type = '';

    /**
     * @var string
     *
     * @ORM\Column(name="details", type="text", length=65535, nullable=true)
     */
    private $details;

    /**
     * @var integer
     *
     * @ORM\Column(name="executed_at", type="integer", nullable=true)
     */
    private $executedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="executed_by", type="integer", nullable=true)
     */
    private $executedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="text", length=65535, nullable=true)
     */
    private $source;



    /**
     * Get historyId
     *
     * @return integer
     */
    public function getHistoryId()
    {
        return $this->historyId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return WpWysijaUserHistory
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set emailId
     *
     * @param integer $emailId
     *
     * @return WpWysijaUserHistory
     */
    public function setEmailId($emailId)
    {
        $this->emailId = $emailId;

        return $this;
    }

    /**
     * Get emailId
     *
     * @return integer
     */
    public function getEmailId()
    {
        return $this->emailId;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return WpWysijaUserHistory
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set details
     *
     * @param string $details
     *
     * @return WpWysijaUserHistory
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set executedAt
     *
     * @param integer $executedAt
     *
     * @return WpWysijaUserHistory
     */
    public function setExecutedAt($executedAt)
    {
        $this->executedAt = $executedAt;

        return $this;
    }

    /**
     * Get executedAt
     *
     * @return integer
     */
    public function getExecutedAt()
    {
        return $this->executedAt;
    }

    /**
     * Set executedBy
     *
     * @param integer $executedBy
     *
     * @return WpWysijaUserHistory
     */
    public function setExecutedBy($executedBy)
    {
        $this->executedBy = $executedBy;

        return $this;
    }

    /**
     * Get executedBy
     *
     * @return integer
     */
    public function getExecutedBy()
    {
        return $this->executedBy;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return WpWysijaUserHistory
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }
}
