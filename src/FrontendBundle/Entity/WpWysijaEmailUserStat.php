<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpWysijaEmailUserStat
 *
 * @ORM\Table(name="wp_wysija_email_user_stat")
 * @ORM\Entity
 */
class WpWysijaEmailUserStat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="email_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $emailId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sent_at", type="integer", nullable=false)
     */
    private $sentAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="opened_at", type="integer", nullable=true)
     */
    private $openedAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status = '0';



    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return WpWysijaEmailUserStat
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set emailId
     *
     * @param integer $emailId
     *
     * @return WpWysijaEmailUserStat
     */
    public function setEmailId($emailId)
    {
        $this->emailId = $emailId;

        return $this;
    }

    /**
     * Get emailId
     *
     * @return integer
     */
    public function getEmailId()
    {
        return $this->emailId;
    }

    /**
     * Set sentAt
     *
     * @param integer $sentAt
     *
     * @return WpWysijaEmailUserStat
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * Get sentAt
     *
     * @return integer
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * Set openedAt
     *
     * @param integer $openedAt
     *
     * @return WpWysijaEmailUserStat
     */
    public function setOpenedAt($openedAt)
    {
        $this->openedAt = $openedAt;

        return $this;
    }

    /**
     * Get openedAt
     *
     * @return integer
     */
    public function getOpenedAt()
    {
        return $this->openedAt;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return WpWysijaEmailUserStat
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
}
