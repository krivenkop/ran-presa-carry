<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpWysijaUserList
 *
 * @ORM\Table(name="wp_wysija_user_list")
 * @ORM\Entity
 */
class WpWysijaUserList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="list_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $listId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sub_date", type="integer", nullable=true)
     */
    private $subDate = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="unsub_date", type="integer", nullable=true)
     */
    private $unsubDate = '0';



    /**
     * Set listId
     *
     * @param integer $listId
     *
     * @return WpWysijaUserList
     */
    public function setListId($listId)
    {
        $this->listId = $listId;

        return $this;
    }

    /**
     * Get listId
     *
     * @return integer
     */
    public function getListId()
    {
        return $this->listId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return WpWysijaUserList
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set subDate
     *
     * @param integer $subDate
     *
     * @return WpWysijaUserList
     */
    public function setSubDate($subDate)
    {
        $this->subDate = $subDate;

        return $this;
    }

    /**
     * Get subDate
     *
     * @return integer
     */
    public function getSubDate()
    {
        return $this->subDate;
    }

    /**
     * Set unsubDate
     *
     * @param integer $unsubDate
     *
     * @return WpWysijaUserList
     */
    public function setUnsubDate($unsubDate)
    {
        $this->unsubDate = $unsubDate;

        return $this;
    }

    /**
     * Get unsubDate
     *
     * @return integer
     */
    public function getUnsubDate()
    {
        return $this->unsubDate;
    }
}
