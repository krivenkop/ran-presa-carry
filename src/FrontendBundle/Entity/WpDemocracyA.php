<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpDemocracyA
 *
 * @ORM\Table(name="wp_democracy_a", indexes={@ORM\Index(name="qid", columns={"qid"})})
 * @ORM\Entity
 */
class WpDemocracyA
{
    /**
     * @var integer
     *
     * @ORM\Column(name="aid", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $aid;

    /**
     * @var integer
     *
     * @ORM\Column(name="qid", type="bigint", nullable=false)
     */
    private $qid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="text", length=65535, nullable=false)
     */
    private $answer;

    /**
     * @var integer
     *
     * @ORM\Column(name="votes", type="integer", nullable=false)
     */
    private $votes = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="aorder", type="integer", nullable=false)
     */
    private $aorder = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="added_by", type="string", length=100, nullable=false)
     */
    private $addedBy = '';



    /**
     * Get aid
     *
     * @return integer
     */
    public function getAid()
    {
        return $this->aid;
    }

    /**
     * Set qid
     *
     * @param integer $qid
     *
     * @return WpDemocracyA
     */
    public function setQid($qid)
    {
        $this->qid = $qid;

        return $this;
    }

    /**
     * Get qid
     *
     * @return integer
     */
    public function getQid()
    {
        return $this->qid;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return WpDemocracyA
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set votes
     *
     * @param integer $votes
     *
     * @return WpDemocracyA
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;

        return $this;
    }

    /**
     * Get votes
     *
     * @return integer
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Set aorder
     *
     * @param integer $aorder
     *
     * @return WpDemocracyA
     */
    public function setAorder($aorder)
    {
        $this->aorder = $aorder;

        return $this;
    }

    /**
     * Get aorder
     *
     * @return integer
     */
    public function getAorder()
    {
        return $this->aorder;
    }

    /**
     * Set addedBy
     *
     * @param string $addedBy
     *
     * @return WpDemocracyA
     */
    public function setAddedBy($addedBy)
    {
        $this->addedBy = $addedBy;

        return $this;
    }

    /**
     * Get addedBy
     *
     * @return string
     */
    public function getAddedBy()
    {
        return $this->addedBy;
    }
}
