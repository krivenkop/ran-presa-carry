<?php

namespace FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpWysijaUser
 *
 * @ORM\Table(name="wp_wysija_user", uniqueConstraints={@ORM\UniqueConstraint(name="EMAIL_UNIQUE", columns={"email"})})
 * @ORM\Entity
 */
class WpWysijaUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="wpuser_id", type="integer", nullable=false)
     */
    private $wpuserId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=false)
     */
    private $firstname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=false)
     */
    private $lastname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=100, nullable=false)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="confirmed_ip", type="string", length=100, nullable=false)
     */
    private $confirmedIp = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="confirmed_at", type="integer", nullable=true)
     */
    private $confirmedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_opened", type="integer", nullable=true)
     */
    private $lastOpened;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_clicked", type="integer", nullable=true)
     */
    private $lastClicked;

    /**
     * @var string
     *
     * @ORM\Column(name="keyuser", type="string", length=255, nullable=false)
     */
    private $keyuser = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="integer", nullable=true)
     */
    private $createdAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=255, nullable=true)
     */
    private $domain = '';



    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set wpuserId
     *
     * @param integer $wpuserId
     *
     * @return WpWysijaUser
     */
    public function setWpuserId($wpuserId)
    {
        $this->wpuserId = $wpuserId;

        return $this;
    }

    /**
     * Get wpuserId
     *
     * @return integer
     */
    public function getWpuserId()
    {
        return $this->wpuserId;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return WpWysijaUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return WpWysijaUser
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return WpWysijaUser
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return WpWysijaUser
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set confirmedIp
     *
     * @param string $confirmedIp
     *
     * @return WpWysijaUser
     */
    public function setConfirmedIp($confirmedIp)
    {
        $this->confirmedIp = $confirmedIp;

        return $this;
    }

    /**
     * Get confirmedIp
     *
     * @return string
     */
    public function getConfirmedIp()
    {
        return $this->confirmedIp;
    }

    /**
     * Set confirmedAt
     *
     * @param integer $confirmedAt
     *
     * @return WpWysijaUser
     */
    public function setConfirmedAt($confirmedAt)
    {
        $this->confirmedAt = $confirmedAt;

        return $this;
    }

    /**
     * Get confirmedAt
     *
     * @return integer
     */
    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    /**
     * Set lastOpened
     *
     * @param integer $lastOpened
     *
     * @return WpWysijaUser
     */
    public function setLastOpened($lastOpened)
    {
        $this->lastOpened = $lastOpened;

        return $this;
    }

    /**
     * Get lastOpened
     *
     * @return integer
     */
    public function getLastOpened()
    {
        return $this->lastOpened;
    }

    /**
     * Set lastClicked
     *
     * @param integer $lastClicked
     *
     * @return WpWysijaUser
     */
    public function setLastClicked($lastClicked)
    {
        $this->lastClicked = $lastClicked;

        return $this;
    }

    /**
     * Get lastClicked
     *
     * @return integer
     */
    public function getLastClicked()
    {
        return $this->lastClicked;
    }

    /**
     * Set keyuser
     *
     * @param string $keyuser
     *
     * @return WpWysijaUser
     */
    public function setKeyuser($keyuser)
    {
        $this->keyuser = $keyuser;

        return $this;
    }

    /**
     * Get keyuser
     *
     * @return string
     */
    public function getKeyuser()
    {
        return $this->keyuser;
    }

    /**
     * Set createdAt
     *
     * @param integer $createdAt
     *
     * @return WpWysijaUser
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return integer
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return WpWysijaUser
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return WpWysijaUser
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }
}
