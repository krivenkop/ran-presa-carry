<?php

namespace FrontendBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class FrontendBundle extends Bundle
{
}
