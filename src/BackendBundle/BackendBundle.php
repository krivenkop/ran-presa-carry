<?php

namespace BackendBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class BackendBundle extends Bundle
{
}
