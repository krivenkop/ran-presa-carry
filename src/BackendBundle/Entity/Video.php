<?php

namespace BackendBundle\Entity;

use SeoBundle\Entity\SeoTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Video
 *
 * @Gedmo\Loggable
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="video_table", uniqueConstraints={
@ORM\UniqueConstraint(name="seo_UNIQUE", columns={"seo_id"})
 *     }, indexes={
@ORM\Index(name="seo_idx", columns={"seo_id"}),
@ORM\Index(name="position_idx", columns={"position"}),
@ORM\Index(name="show_on_website_idx", columns={"show_on_website"}),
 *     })
 * @ORM\Entity(repositoryClass="BackendBundle\Entity\Repository\VideoRepository")
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class Video
{
    /* YES / NO */
    public const YES = 1;
    public const NO = 0;

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable;

    use SeoTrait;

    /**
     * @param $method
     * @param $arguments
     * @return mixed|null
     */
    public function __call($method, $arguments)
    {
        if ($method == '_action') {
            return null;
        }

        return PropertyAccess::createPropertyAccessor()->getValue($this->translate(), $method);
    }

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @Gedmo\Versioned
     */
    private $id;

    /**
     * @var integer
     *
     * @Gedmo\Versioned
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    protected $position;

    /**
     * @var boolean
     *
     * @Gedmo\Versioned
     * @Assert\NotBlank()
     * @ORM\Column(name="show_on_website", type="integer", nullable=false)
     */
    protected $showOnWebsite = self::YES;  # показывать на сайте: 0 - нет, 1 - да

    /**
     * @var string
     *
     * @Gedmo\Versioned
     * @ORM\Column(name="iframe", type="text", nullable=true)
     */
    private $iframe;

    public function __toString()
    {
        return (string)$this->translate()->getTitle();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getShowOnWebsite(): int
    {
        return $this->showOnWebsite;
    }

    /**
     * @param int $showOnWebsite
     */
    public function setShowOnWebsite(int $showOnWebsite): void
    {
        $this->showOnWebsite = $showOnWebsite;
    }

    /**
     * @return array|mixed
     */
    public static function yesOrNo()
    {
        return [
            self::YES => "form.yes",
            self::NO => "form.no"
        ];
    }

    /**
     * @return array|mixed
     */
    public static function yesOrNoForm()
    {
        return [
            "form.yes" => self::YES,
            "form.no" => self::NO,
        ];
    }

    /**
     * @return string
     */
    public function getIframe(): ?string
    {
        return $this->iframe;
    }

    /**
     * @param string $iframe
     */
    public function setIframe(?string $iframe): void
    {
        $this->iframe = $iframe;
    }
}