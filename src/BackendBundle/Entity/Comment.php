<?php

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use NewsBundle\Entity\News;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Comment
 * @ORM\Table(name="comment_table")
 * @ORM\Entity(repositoryClass="BackendBundle\Entity\Repository\CommentRepository")
 */
class Comment
{
    /* YES / NO */
    const YES = 1;
    const NO = 0;
    /**
     * @var \DateTimeInterface|null
     *
     * @ORM\Column(name="publish_at", type="datetime", nullable=true)
     */
    protected $publishAt;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="show_on_website", type="integer", nullable=false)
     */
    private $showOnWebsite = self::YES;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;
    /**
     * @ORM\ManyToOne(targetEntity="NewsBundle\Entity\News", inversedBy="comments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="news_id", referencedColumnName="id", onDelete="SET NULL")
     * })
     */
    private $news;

    /**
     * @return array|mixed
     */
    public static function yesOrNo()
    {
        return [
            self::YES => "Да",
            self::NO => "Нет"
        ];
    }

    /**
     * @return array|mixed
     */
    public static function yesOrNoForm()
    {
        return [
            "form.yes" => self::YES,
            "form.no" => self::NO,
        ];
    }

    public function __toString()
    {
        return (string)$this->translate()->getTitle();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return $this
     */
    public function setContent(string $content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get showOnWebsite
     *
     * @return integer
     */
    public function getShowOnWebsite()
    {
        return $this->showOnWebsite;
    }

    /**
     * Set showOnWebsite
     *
     * @param integer $showOnWebsite
     *
     * @return $this
     */
    public function setShowOnWebsite($showOnWebsite)
    {
        $this->showOnWebsite = $showOnWebsite;

        return $this;
    }

    /**
     * @return News
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * @param News $news
     */
    public function setNews(News $news)
    {
        $this->news = $news;
    }

    public function getPublishAt()
    {
        if (!$this->publishAt) {
            self::setPublishAt(null);
        }

        return $this->publishAt->format('d-m-Y');
    }
    public function getPublishAtWithTime()
    {
        if (!$this->publishAt) {
            self::setPublishAt(null);
        }

        return $this->publishAt->format('H:i, Y-m-d');
    }


    public function setPublishAt($publishAt): void
    {
        if (is_null($publishAt)) {
            $this->publishAt = new \DateTime();
        } else {
            $this->publishAt = new \DateTime($publishAt);
        }
    }
}

