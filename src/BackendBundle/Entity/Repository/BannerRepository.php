<?php

namespace BackendBundle\Entity\Repository;

use BackendBundle\Entity\Banner;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;
use DashboardBundle\Entity\Repository\DashboardRepository;

class BannerRepository extends DashboardRepository
{
    public function allElementsForIndexDashboard(Request $request)
    {
        $query = self::getQuery();
        $query->select('q, t');

        foreach ($request->get('order') as $order) {
            if ($order['column'] == 1) {
                $query->addOrderBy('q.id', $order['dir']);
            }
        }

        return $query->getQuery();
    }

    public function getElementByIdForDashboardEditFormOrDeleteAction($id)
    {
        $query = self::getQuery();
        $query->select('q, t')
            ->where('q.id =:id')
            ->setParameter('id', $id);

        return $query->getQuery()->getOneOrNullResult();
    }

    private function getQuery()
    {
        $query = $this->createQueryBuilder('q')
            ->leftJoin('q.translations', 't');

        return $query;
    }

    public function getByPage($page)
    {
        $query = $this->createQueryBuilder('s', 's.linkName')
            ->select('s, t')
            ->where('s.page = :page')
            ->leftJoin('s.translations', 't')
            ->setParameter('page', $page)
            ->andWhere('s.showOnWebsite = :showOnWebsite')
            ->setParameter('showOnWebsite', Banner::YES)
            ->getQuery()
            ->getResult();

        return $query;
    }
}
