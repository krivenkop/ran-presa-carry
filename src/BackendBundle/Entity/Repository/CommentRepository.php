<?php

namespace BackendBundle\Entity\Repository;

use BackendBundle\Entity\Banner;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;
use DashboardBundle\Entity\Repository\DashboardRepository;

class CommentRepository extends DashboardRepository
{
    public function allElementsForIndexDashboard(Request $request)
    {
        $query = self::getQuery();
        $query->select('q');

        foreach ($request->get('order') as $order) {
            if ($order['column'] == 1) {
                $query->addOrderBy('q.id', $order['dir']);
            }
        }

        return $query->getQuery();
    }

    public function getElementByIdForDashboardEditFormOrDeleteAction($id)
    {
        $query = self::getQuery();
        $query->select('q')
            ->where('q.id =:id')
            ->setParameter('id', $id);

        return $query->getQuery()->getOneOrNullResult();
    }

    private function getQuery()
    {
        $query = $this->createQueryBuilder('q');

        return $query;
    }

    public function getByPage($page)
    {
        $query = $this->createQueryBuilder('s', 's.linkName')
            ->select('s')
            ->where('s.page = :page')
            ->setParameter('page', $page)
            ->andWhere('s.showOnWebsite = :showOnWebsite')
            ->setParameter('showOnWebsite', Banner::YES)
            ->getQuery()
            ->getResult();

        return $query;
    }
}
