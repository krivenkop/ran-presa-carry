<?php

namespace BackendBundle\Entity\Repository;

use DashboardBundle\Entity\Repository\DashboardRepository;
use Symfony\Component\HttpFoundation\Request;
use BackendBundle\Entity\Video;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class InfoRepository extends DashboardRepository
{
    private function createQuery()
    {
        $query = $this->createQueryBuilder('q')
            ->select('q, t, seo, seo_t')
            ->leftJoin('q.translations', 't')
            ->leftJoin('q.seo', 'seo')
            ->leftJoin('seo.translations', 'seo_t');

        return $query;
    }

    public function allElementsForIndexDashboard(Request $request)
    {
        $query = self::createQuery();

        foreach ($request->get('order') as $order) {
            if ($order['column'] == 1) {
                $query->addOrderBy('q.id', $order['dir']);
            } elseif ($order['column'] == 2) {
                $query->addOrderBy('t.title', $order['dir']);
            }
        }

        return $query->getQuery()->getResult();
    }

    public function getElementByIdForDashboardEditFormOrDeleteAction($id)
    {
        $query = self::createQuery();
        $query
            ->where('q.id =:id')
            ->setParameter('id', $id);

        return $query->getQuery()->getOneOrNullResult();
    }


    public function getLimitElements($count)
    {
        $query = self::createQuery();
        $query
            ->addOrderBy('q.position', 'DESC')
            ->andWhere('q.showOnWebsite =:showOnWebsite')
            ->setParameters([
                'showOnWebsite' => Video::YES,
            ]);

        if (!is_null($count)) {
            $query->setMaxResults($count);
        }

        return $query->getQuery()->getResult();
    }

    public function getByIdsForSearch($ids, $isOnlyQuery)
    {
        $query = self::createQuery();
        $query
            ->andWhere('q.id IN (:ids)')
            ->addOrderBy('q.position', 'DESC')
            ->andWhere('q.showOnWebsite =:showOnWebsite')
            ->setParameters([
                'ids' => $ids,
                'showOnWebsite' => Video::YES
            ]);

        $results = $query->getQuery();

        if (!$isOnlyQuery) {
            $results = $query->getQuery()->getResult();
        }

        return $results;
    }
}