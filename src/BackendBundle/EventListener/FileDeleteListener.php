<?php

namespace BackendBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use UploadBundle\Services\FileHandler;

class FileDeleteListener
{
    private $elements;
    private $fileHandler;

    public function __construct(FileHandler $fileHandler)
    {
        $this->fileHandler = $fileHandler;
        $config = new FileUpdateConfig;
        $this->elements = $config->elements;
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $shortClass = explode("\\", get_class($entity));
        $shortClass = end($shortClass);
        if (isset($this->elements[$shortClass])) {
            $this->handleFiles($this->elements[$shortClass], $entity);
        }
        return;
    }

    private function handleFiles($arr, $entity)
    {
        $option = array_values($arr)[0];
        $getter = 'get' . ucfirst($option);
        $data = @json_decode($entity->$getter(), true);
        if (@$data['default_file']) {
            $path = array_slice(explode('/', $data['default_file']), 1, -1);
            $str = null;
            foreach ($path as $p) {
                $str .= '/' . $p;
            }
            $this->fileHandler->clearDirectory($str, true);
        }
    }
}