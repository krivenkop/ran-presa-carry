<?php

namespace BackendBundle\Form\Type;

use SeoBundle\Form\Type\Dashboard\SeoType;
use Symfony\Component\Security\Core\Security;
use UploadBundle\Form\UpbeatUploadType;
use DashboardBundle\Form\Type\Dashboard\DashboardDatetimeType;
use DashboardBundle\Form\Type\Dashboard\DashboardDescriptionType;
use DashboardBundle\Form\Type\Dashboard\DashboardInteger2Type;
use DashboardBundle\Form\Type\Dashboard\DashboardSelect2EntityType;
use DashboardBundle\Form\Type\Dashboard\DashboardSelectType;
use DashboardBundle\Form\Type\Dashboard\DashboardTextareaType;
use DashboardBundle\Form\Type\Dashboard\DashboardTextType;
use Doctrine\ORM\EntityRepository;
use DashboardBundle\Form\Type\Dashboard\DashboardTranslationsType;
use BackendBundle\Entity\Video;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class VideoType extends AbstractType
{
    protected $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', DashboardTranslationsType::class, [
                'label' => false,
                'fields' => [
                    'title' => [
                        'field_type' => DashboardTextType::class,
                        'label' => 'form.title',
                        'help_block' => null,
                        'max_length' => 255
                    ],
                ]
            ])
            ->add('seo', SeoType::class)
            ->add('iframe', DashboardTextareaType::class, [
                'label' => 'iframe url',
                'required' => false,
            ])
            ->add('position', DashboardInteger2Type::class, [
                'label' => 'form.position',
                'up_color' => 'red',
                'down_color' => 'green',
            ])
            ->add('showOnWebsite', DashboardSelectType::class, [
                'label' => 'form.show_on_website',
                'translation_domain' => 'DashboardBundle',
                'choices' => Video::yesOrNoForm()
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Video::class, 'request' => null]);
    }
}