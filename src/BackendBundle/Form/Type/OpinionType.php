<?php

namespace BackendBundle\Form\Type;

use BackendBundle\Entity\Opinion;
use DashboardBundle\Form\Type\Dashboard\DashboardDescriptionType;
use SeoBundle\Form\Type\Dashboard\SeoType;
use DashboardBundle\Form\Type\Dashboard\DashboardInteger2Type;
use DashboardBundle\Form\Type\Dashboard\DashboardSelectType;
use DashboardBundle\Form\Type\Dashboard\DashboardTextareaType;
use DashboardBundle\Form\Type\Dashboard\DashboardTextType;
use DashboardBundle\Form\Type\Dashboard\DashboardTranslationsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UploadBundle\Form\UpbeatUploadType;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class OpinionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', DashboardTranslationsType::class, [
                'label' => false,
                'fields' => [
                    'title' => [
                        'field_type' => DashboardTextType::class,
                        'label' => 'form.title',
                        'help_block' => null,
                        'max_length' => 255
                    ],
                    'shortDescription' => [
                        'field_type' => DashboardTextareaType::class,
                        'label' => 'form.short_description',
                        'required' => false,
                    ],
                    'description' => [
                        'field_type' => DashboardDescriptionType::class,
                        'attr' => [
                            'class' => 'tinymce',
                            'data-theme' => 'medium'
                        ],
                        'required' => false,
                        'label' => 'form.description',
                    ]
                ]
            ])
            ->add('seo', SeoType::class)
            ->add('poster', UpbeatUploadType::class, [
                'file_type' => 'opinion_poster',
                'extensions' => 'jpg,gif,png,svg',
                'crop' => false,
                'label' => 'form.poster',
                'required' => false,
            ])
            ->add('position', DashboardInteger2Type::class, [
                'label' => 'form.position',
                'up_color' => 'red',
                'down_color' => 'green',
            ])
            ->add('showOnWebsite', DashboardSelectType::class, [
                'label' => 'form.show_on_website',
                'translation_domain' => 'DashboardBundle',
                'choices' => Opinion::yesOrNoForm()
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Opinion::class, 'request' => null]);
    }
}