<?php

namespace BackendBundle\Form\Type;

use BackendBundle\Entity\Banner;
use BackendBundle\Entity\Comment;
use DashboardBundle\Form\Type\Dashboard\DashboardDatetimeType;
use DashboardBundle\Form\Type\Dashboard\DashboardSelect2EntityType;
use DashboardBundle\Form\Type\Dashboard\DashboardTextareaType;
use NewsBundle\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use DashboardBundle\Form\Type\Dashboard\DashboardTextType;
use DashboardBundle\Form\Type\Dashboard\DashboardSelectType;
use DashboardBundle\Form\Type\Dashboard\DashboardTranslationsType;
use UploadBundle\Form\UpbeatUploadType;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', DashboardTextType::class, [
                'label' => 'Имя посетителя',
                'help_block' => null,
                'max_length' => 255,
                'required' => false,
            ])
            ->add('content', DashboardTextareaType::class, [
                'label' => 'Контент комментария',
                'help_block' => null,
                'max_length' => 3000,
                'required' => false,
            ])
            ->add('showOnWebsite', DashboardSelectType::class, [
                'label' => 'Показывать на сайте',
                'choices' => Banner::yesOrNoForm()
            ])
            ->add('news', DashboardSelect2EntityType::class, [
                'class' => News::class,
                'attr' => [
                    'disabled' => 'disabled'
                ]
            ])
            ->add('publishAt', DashboardDatetimeType::class, [
                'label' => 'Опубликован'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Comment::class, 'request' => null]);
    }
}