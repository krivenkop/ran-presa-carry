<?php

namespace BackendBundle\Form\Type;

use BackendBundle\Entity\Banner;
use BackendBundle\Entity\Comment;
use DashboardBundle\Form\Type\Dashboard\DashboardDatetimeType;
use DashboardBundle\Form\Type\Dashboard\DashboardSelect2EntityType;
use DashboardBundle\Form\Type\Dashboard\DashboardTextareaType;
use NewsBundle\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use DashboardBundle\Form\Type\Dashboard\DashboardTextType;
use DashboardBundle\Form\Type\Dashboard\DashboardSelectType;
use DashboardBundle\Form\Type\Dashboard\DashboardTranslationsType;
use UploadBundle\Form\UpbeatUploadType;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class StoreCommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'МОЕ ИМЯ И ФАМИЛИЯ*',
                'required' => true,
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Я ПИШУ*',
                'required' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Comment::class, 'request' => null]);
    }
}