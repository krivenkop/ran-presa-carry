<?php

namespace BackendBundle\Form\Type;

use BackendBundle\Entity\Banner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use DashboardBundle\Form\Type\Dashboard\DashboardTextType;
use DashboardBundle\Form\Type\Dashboard\DashboardSelectType;
use DashboardBundle\Form\Type\Dashboard\DashboardTranslationsType;
use UploadBundle\Form\UpbeatUploadType;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class BannerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', DashboardTranslationsType::class, [
                'label' => false,
                'fields' => [
                    'title' => [
                        'label' => 'Название',
                        'field_type' => DashboardTextType::class,
                        'help_block' => null,
                        'max_length' => 255
                    ],
                ]
            ])
            ->add('logo', UpbeatUploadType::class, [
                'file_type' => 'banner_logo',
                'extensions' => 'jpg,gif,png,svg',
                'crop' => false,
                'label' => 'form.poster',
                'required' => false,
            ])
            ->add('url', DashboardTextType::class, [
                'label' => 'Ссылка',
                'help_block' => null,
                'max_length' => 255,
                'required' => false,
            ])
            ->add('showOnWebsite', DashboardSelectType::class, [
                'label' => 'form.show_on_website',
                'translation_domain' => 'DashboardBundle',
                'choices' => Banner::yesOrNoForm()
            ])
            ->add('linkName', DashboardTextType::class, [
                'label' => 'form.static_content.link_name',
                'help_block' => null,
                'max_length' => 255
            ])
            ->add('page', DashboardTextType::class, [
                'help_block' => null,
                'max_length' => 255,
                'label' => 'form.static_content.page',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Banner::class, 'request' => null]);
    }
}