<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Banner;
use BackendBundle\Form\Type\BannerType;
use DashboardBundle\Controller\CRUDController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class BannerController extends CRUDController
{
    public function getHeadTitle(TranslatorInterface $translator): string
    {
        return 'Баннеры';
    }

    public function getGrantedRoles(): array
    {
        return [
            'index' => 'ROLE_ADMIN_PANEL', 'new' => 'ROLE_ADMIN_PANEL',
            'edit' => 'ROLE_ADMIN_PANEL', 'delete' => 'ROLE_ADMIN_PANEL',
        ];
    }

    public function getRouteElements(): array
    {
        return [
            'index' => 'dashboard_banner_index', 'new' => 'dashboard_banner_new',
            'edit' => 'dashboard_banner_edit', 'delete' => 'dashboard_banner_delete',
        ];
    }

    public function getCaptionSubjectIcon(): string
    {
        return 'icon-folder';
    }

    public function getFormType(): string
    {
        return BannerType::class;
    }

    public function getFormElement()
    {
        $banner = new Banner();
        return $banner;
    }

    public function getRepository(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Banner::class);

        return $repository;
    }


    public function getListElementsForIndexDashboard(TranslatorInterface $translator): array
    {
        return [
            'title' => $translator->trans('form.title', [], 'DashboardBundle'),
            'poster' => $translator->trans('form.poster', [], 'DashboardBundle'),
            'showOnWebsite' => $translator->trans('form.show_on_website', [], 'DashboardBundle'),
        ];
    }

    public function getConfigForIndexDashboard(): array
    {
        return [
            'pageLength' => 25,
            'lengthMenu' => '10, 20, 25, 50, 100, 150',
            'order_column' => 1,
            'order_by' => "asc"
        ];
    }

    public function getElementsForIndexDashboard(Request $request, EntityManagerInterface $em, TranslatorInterface $translator, Environment $twig)
    {

        $repository = $this->getRepository($em);

        $iTotalRecords = $repository->countAllElementsForIndexDashboard();
        $elements = $repository->allElementsForIndexDashboard($request);
        $helper = $this->dashboardManager->helperForIndexDashboard($iTotalRecords, $elements);
        $pagination = $helper['pagination'];
        $records = $helper['records'];


        foreach ($pagination as $element) {
            $records["data"][] = [
                $twig->render('@Dashboard/default/list/_checkbox.html.twig', ['id' => $element->getId()]),
                $element->getId(),
                $element->translate()->getTitle(),
                $twig->render('@Dashboard/default/list/_img.html.twig', ['element' => $element->getLogo()]),
                $twig->render('@Dashboard/default/list/_yes_no.html.twig', ['element' => $element->getShowOnWebsite()]),
                $twig->render('@Dashboard/default/list/_actions_edit_delete.html.twig', [
                    'action_edit_url' => $this->generateUrl($this->getRouteElements()['edit'], ['id' => $element->getId()]),
                    'action_delete_url' => $this->generateUrl($this->getRouteElements()['delete'], ['id' => $element->getId()]),
                    'action_edit_role' => $this->getGrantedRoles()['edit'],
                    'action_delete_role' => $this->getGrantedRoles()['delete'],
                ]),
            ];
        }

        return $records;
    }

    public function getPortletBodyTemplateForForm(): string
    {
        return '@Backend/dashboard/banner/form/_portlet_body.html.twig';
    }
}