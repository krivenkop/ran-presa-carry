<?php

namespace BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use SeoBundle\Controller\Dashboard\sidebarSeoBundleTrait;
use StaticBundle\Controller\Dashboard\sidebarStaticBundleTrait;
use Doctrine\ORM\EntityManagerInterface;
use NewsBundle\Controller\Dashboard\sidebarNewsBundleTrait;
use SupportCenter\ContactBundle\Controller\Dashboard\sidebarContactBundleTrait;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
final class DashboardController extends \DashboardBundle\Controller\DashboardController
{
    use sidebarSeoBundleTrait;
    use sidebarStaticBundleTrait;
    use sidebarNewsBundleTrait;
    use sidebarContactBundleTrait;

    public function index(EntityManagerInterface $em)
    {
        return $this->render('@Backend/dashboard/homepage/index.html.twig', [
//            'countNewOrders' => $em->getRepository(Order::class)->countNewOrders(),
//            'countNewContactsRequests' => $em->getRepository(Contact::class)->countNewContactsRequests(),
//            'countNewCallbackRequests' => $em->getRepository(Callback::class)->countNewCallbackRequests(),
        ]);
    }

    public function sidebar(EntityManagerInterface $em, Request $request)
    {
        $sidebar = [];

        $sidebar['support_center'] = self::headingSidebar(
            'icon-question',
            'icon-info',
            'sidebar.support_center', [
            'ROLE_CONTACT', 'ROLE_CONTACT_STATUS_CREATE_EDIT',
            'ROLE_CONTACT_MANAGER_CREATE_EDIT', 'ROLE_CONTACT_MAIL_SETTING_EDIT',
            'ROLE_CONTACT_PHONE_CREATE_EDIT',
        ], []);

        $sidebar['support_center']['items'][] = self::sidebarContactBundle($em);
        $sidebar['news'] = self::sidebarNewsBundle();

        $sidebar['video'] = self::headingSidebar('icon-social-youtube', '', 'Видео', ['ROLE_VIDEO_CREATE_EDIT'], [
            self::itemSidebar(['ROLE_VIDEO_CREATE_EDIT'], ['video/edit/'], ['dashboard_video_index', 'dashboard_video_new'], 'icon-social-youtube', false, null, null, 'Видео', [], 'dashboard_video_index'),
            ]);

        $sidebar['info'] = self::headingSidebar('icon-folder', '', 'ИНФОГРАФИКА', ['ROLE_ADMIN_PANEL'], [
            self::itemSidebar(['ROLE_ADMIN_PANEL'], ['info/edit/'], ['dashboard_info_index', 'dashboard_info_new'], 'icon-folder', false, null, null, 'Инфографика', [], 'dashboard_info_index'),
        ]);

        $sidebar['opinion'] = self::headingSidebar('icon-folder', '', 'МНЕНИЯ', ['ROLE_ADMIN_PANEL'], [
            self::itemSidebar(['ROLE_ADMIN_PANEL'], ['opinion/edit/'], ['dashboard_opinion_index', 'dashboard_opinion_new'], 'icon-folder', false, null, null, 'Мнения', [], 'dashboard_opinion_index'),
        ]);

        $sidebar['banner'] = self::headingSidebar('fa fa-picture-o', '', 'Баннеры', ['ROLE_ADMIN_PANEL'], []);

        $sidebar['banner']['items'][] = self::itemSidebar(
            ['ROLE_ADMIN_PANEL'], ['banner/edit/'], ['dashboard_banner_index', 'dashboard_banner_new'], 'fa fa-picture-o',
            false, null, null, 'Баннеры', [], 'dashboard_banner_index');

        $sidebar['comment'] = self::headingSidebar('fa fa-comment', '', 'Комментарии', ['ROLE_ADMIN_PANEL'], []);

        $sidebar['comment']['items'][] = self::itemSidebar(
            ['ROLE_ADMIN_PANEL'], ['сomment/edit/'], ['dashboard_comment_index', 'dashboard_comment_new'], 'fa fa-comment',
            false, null, null, 'Комментарии', [], 'dashboard_comment_index');

        $sidebar['settings'] = self::headingSidebar(
            'icon-settings', 'icon-wrench', 'sidebar.configuration.configuration',
            ['ROLE_SEO', 'ROLE_STATIC', 'ROLE_SYNCHRONIZATION_DELOVOD', 'ROLE_IMPORT_BY_WAYBILL_NUMBER', 'ROLE_LOG'], [
////            self::itemSidebar(['ROLE_LOG'], null, ['dashboard_log'], 'icon-info', false, null, null, 'sidebar.configuration.log', [], 'dashboard_log')
        ]);

        $sidebar['settings']['items'][] = self::sidebarSeoBundle();
        $sidebar['settings']['items'][] = self::sidebarStaticBundle();

        return $this->render('@Dashboard/default/_sidebar_item_and_heading.html.twig', [
            'sidebar' => $sidebar,
            'request' => $request,
        ]);
    }
}