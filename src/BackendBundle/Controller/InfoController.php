<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Info;
use BackendBundle\Form\Type\InfoType;
use SeoBundle\Entity\Seo;
use DashboardBundle\Controller\CRUDController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class InfoController extends CRUDController
{
    public function getHeadTitle(TranslatorInterface $translator): string
    {
        return 'ИНФОГРАФИКА';
    }

    public function getGrantedRoles(): array
    {
        return [
            'index' => 'ROLE_ADMIN_PANEL', 'new' => 'ROLE_ADMIN_PANEL',
            'edit' => 'ROLE_ADMIN_PANEL', 'delete' => 'ROLE_ADMIN_PANEL',
        ];
    }

    public function getRouteElements(): array
    {
        return [
            'index' => 'dashboard_info_index', 'new' => 'dashboard_info_new',
            'edit' => 'dashboard_info_edit', 'delete' => 'dashboard_info_delete',
        ];
    }

    public function getCaptionSubjectIcon(): string
    {
        return '';
    }

    public function getFormType(): string
    {
        return InfoType::class;
    }

    public function getFormElement()
    {
        $seo = new Seo();
        $new = new Info();
        $new->setSeo($seo);

        return $new;
    }

    public function getRepository(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Info::class);

        return $repository;
    }

    public function getListElementsForIndexDashboard(TranslatorInterface $translator): array
    {
        return [
            'title' => $translator->trans('form.title', [], 'DashboardBundle'),
            'poster' => $translator->trans('form.poster', [], 'DashboardBundle'),
            'position' => $translator->trans('form.position', [], 'DashboardBundle'),
            'showOnWebsite' => $translator->trans('form.show_on_website', [], 'DashboardBundle'),
        ];
    }

    public function getElementsForIndexDashboard(Request $request, EntityManagerInterface $em, TranslatorInterface $translator, Environment $twig)
    {
        $repository = $this->getRepository($em);

        $iTotalRecords = $repository->countAllElementsForIndexDashboard();
        $elements = $repository->allElementsForIndexDashboard($request);

        $helper = $this->dashboardManager->helperForIndexDashboard($iTotalRecords, $elements);
        $pagination = $helper['pagination'];
        $records = $helper['records'];

        foreach ($pagination as $element) {
            $records["data"][] = [
                $twig->render('@Dashboard/default/list/_checkbox.html.twig', ['id' => $element->getId()]),
                $element->getId(),
                $element->translate()->getTitle(),
                $twig->render('@Dashboard/default/list/_img.html.twig', ['element' => $element->getPoster()]),
                $element->getPosition(),
                $twig->render('@Dashboard/default/list/_yes_no.html.twig', ['element' => $element->getShowOnWebsite()]),
                $twig->render('@Dashboard/default/list/_actions_edit_delete.html.twig', [
                    'action_edit_url' => $this->generateUrl($this->getRouteElements()['edit'], ['id' => $element->getId()]),
                    'action_delete_url' => $this->generateUrl($this->getRouteElements()['delete'], ['id' => $element->getId()]),
                    'action_edit_role' => $this->getGrantedRoles()['edit'],
                    'action_delete_role' => $this->getGrantedRoles()['delete'],
                ]),
            ];
        }

        return $records;
    }

    public function getPortletBodyTemplateForForm(): string
    {
        return '@Backend/dashboard/info/form/_portlet_body.html.twig';
    }
}