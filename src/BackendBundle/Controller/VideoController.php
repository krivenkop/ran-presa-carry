<?php

namespace BackendBundle\Controller;

use SeoBundle\Entity\Seo;
use DashboardBundle\Controller\CRUDController;
use BackendBundle\Entity\Video;
use BackendBundle\Form\Type\VideoType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class VideoController extends CRUDController
{
    public function getHeadTitle(TranslatorInterface $translator): string
    {
        return 'Видео';
    }

    public function getGrantedRoles(): array
    {
        return [
            'index' => 'ROLE_VIDEO_LIST', 'new' => 'ROLE_VIDEO_CREATE',
            'edit' => 'ROLE_VIDEO_EDIT', 'delete' => 'ROLE_VIDEO_DELETE',
        ];
    }

    public function getRouteElements(): array
    {
        return [
            'index' => 'dashboard_video_index', 'new' => 'dashboard_video_new',
            'edit' => 'dashboard_video_edit', 'delete' => 'dashboard_video_delete',
        ];
    }

    public function getCaptionSubjectIcon(): string
    {
        return 'icon-social-youtube';
    }

    public function getFormType(): string
    {
        return VideoType::class;
    }

    public function getFormElement()
    {
        $seo = new Seo();
        $new = new Video();
        $new->setSeo($seo);

        return $new;
    }

    public function getRepository(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Video::class);

        return $repository;
    }

    public function getListElementsForIndexDashboard(TranslatorInterface $translator): array
    {
        return [
            'title' => $translator->trans('form.title', [], 'DashboardBundle'),
            'position' => $translator->trans('form.position', [], 'DashboardBundle'),
            'showOnWebsite' => $translator->trans('form.show_on_website', [], 'DashboardBundle'),
        ];
    }

    public function getElementsForIndexDashboard(Request $request, EntityManagerInterface $em, TranslatorInterface $translator, Environment $twig)
    {
        $repository = $this->getRepository($em);

        $iTotalRecords = $repository->countAllElementsForIndexDashboard();
        $elements = $repository->allElementsForIndexDashboard($request);

        $helper = $this->dashboardManager->helperForIndexDashboard($iTotalRecords, $elements);
        $pagination = $helper['pagination'];
        $records = $helper['records'];

        foreach ($pagination as $element) {
            $records["data"][] = [
                $twig->render('@Dashboard/default/list/_checkbox.html.twig', ['id' => $element->getId()]),
                $element->getId(),
                $element->translate()->getTitle(),
                $element->getPosition(),
                $twig->render('@Dashboard/default/list/_yes_no.html.twig', ['element' => $element->getShowOnWebsite()]),
                $twig->render('@Dashboard/default/list/_actions_edit_delete.html.twig', [
                    'action_edit_url' => $this->generateUrl($this->getRouteElements()['edit'], ['id' => $element->getId()]),
                    'action_delete_url' => $this->generateUrl($this->getRouteElements()['delete'], ['id' => $element->getId()]),
                    'action_edit_role' => $this->getGrantedRoles()['edit'],
                    'action_delete_role' => $this->getGrantedRoles()['delete'],
                ]),
            ];
        }

        return $records;
    }

    public function getPortletBodyTemplateForForm(): string
    {
        return '@Backend/dashboard/video/form/_portlet_body.html.twig';
    }
}