<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Banner;
use BackendBundle\Entity\Comment;
use BackendBundle\Form\Type\BannerType;
use BackendBundle\Form\Type\CommentType;
use DashboardBundle\Controller\CRUDController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Ihor Drevetskyi <ihor.drevetskyi@gmail.com>
 */
class CommentController extends CRUDController
{
    public function getHeadTitle(TranslatorInterface $translator): string
    {
        return 'Комментарий';
    }

    public function getGrantedRoles(): array
    {
        return [
            'index' => 'ROLE_ADMIN_PANEL', 'new' => 'ROLE_ADMIN_PANEL',
            'edit' => 'ROLE_ADMIN_PANEL', 'delete' => 'ROLE_ADMIN_PANEL',
        ];
    }

    public function getRouteElements(): array
    {
        return [
            'index' => 'dashboard_comment_index', 'new' => 'dashboard_comment_new',
            'edit' => 'dashboard_comment_edit', 'delete' => 'dashboard_comment_delete',
        ];
    }

    public function getCaptionSubjectIcon(): string
    {
        return 'fa fa-comment';
    }

    public function getFormType(): string
    {
        return CommentType::class;
    }

    public function getFormElement()
    {
        return new Comment();
    }

    public function getRepository(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Comment::class);

        return $repository;
    }


    public function getListElementsForIndexDashboard(TranslatorInterface $translator): array
    {
        return [
            'name' => 'Имя посетителя',
            'content' => 'Контент комментария',
            'showOnWebsite' => 'Показывать на сайте?',
        ];
    }

    public function getConfigForIndexDashboard(): array
    {
        return [
            'pageLength' => 25,
            'lengthMenu' => '10, 20, 25, 50, 100, 150',
            'order_column' => 1,
            'order_by' => "asc"
        ];
    }

    public function getElementsForIndexDashboard(Request $request, EntityManagerInterface $em, TranslatorInterface $translator, Environment $twig)
    {

        $repository = $this->getRepository($em);

        $iTotalRecords = $repository->countAllElementsForIndexDashboard();
        $elements = $repository->allElementsForIndexDashboard($request);
        $helper = $this->dashboardManager->helperForIndexDashboard($iTotalRecords, $elements);
        $pagination = $helper['pagination'];
        $records = $helper['records'];


        foreach ($pagination as $element) {
            $records["data"][] = [
                $twig->render('@Dashboard/default/list/_checkbox.html.twig', ['id' => $element->getId()]),
                $element->getId(),
                $element->getName(),
                $element->getContent(),
                $twig->render('@Dashboard/default/list/_yes_no.html.twig', ['element' => $element->getShowOnWebsite()]),
                $twig->render('@Dashboard/default/list/_actions_edit_delete.html.twig', [
                    'action_edit_url' => $this->generateUrl($this->getRouteElements()['edit'], ['id' => $element->getId()]),
                    'action_delete_url' => $this->generateUrl($this->getRouteElements()['delete'], ['id' => $element->getId()]),
                    'action_edit_role' => $this->getGrantedRoles()['edit'],
                    'action_delete_role' => $this->getGrantedRoles()['delete'],
                ]),
            ];
        }

        return $records;
    }

    public function getPortletBodyTemplateForForm(): string
    {
        return '@Backend/dashboard/comment/form/_portlet_body.html.twig';
    }
}