<?php

namespace AppBundle\Controller;

use AppBundle\Entity\WpPostmeta;
use AppBundle\Entity\WpPosts;
use AppBundle\Entity\WpTermRelationships;
use AppBundle\Entity\WpTerms;
use AppBundle\Entity\WpTermTaxonomy;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $WpTermTaxonomy = $em->getRepository(WpTermTaxonomy::class)->findBy([
            'taxonomy' => 'category'
        ]);

        $categories = [];
        foreach ($WpTermTaxonomy as $taxonomy) {
            $category = $em->getRepository(WpTerms::class)->findOneBy([
                'termId' => $taxonomy->getTermId()
            ]);
            if (!empty($category) and $category->getName() != 'Экипировка') {
                $categories[$category->getName()] = [
                    'id' => $category->getTermId(),
                    'name' => $category->getName(),
                    'items' => []
                ];
            }
        }
        foreach ($categories as $key => $category) {
            $WpTermRelationships = $em->getRepository(WpTermRelationships::class)->findBy([
                'termTaxonomyId' => $category['id']
            ]);
            foreach ($WpTermRelationships as $item) {
                $post = $em->getRepository(WpPosts::class)->findOneBy(['id' => $item->getObjectId()]);
                if (!empty($post)) {
                    $image = null;
                    $posts = $em->getRepository(WpPosts::class)->findBy([
//                        'metaKey' => '_wp_attached_file',
                        'postParent' => $post->getId()
                    ]);
                    if ($posts) {
                        foreach ($posts as $postsItem) {
                            $WpPostmeta = $em->getRepository(WpPostmeta::class)->findOneBy([
                                'metaKey' => '_wp_attached_file',
                                'postId' => $postsItem->getId()
                            ]);
                            if (!empty($WpPostmeta)) {
                                $image = $WpPostmeta->getMetaValue();
                                break;
                            }
                        }
                    }
                    $postArr = [
                        'postDate' => $post->getPostDate(),
                        'postContent' => $post->getPostContent(),
                        'postTitle' => $post->getPostTitle(),
                        'postExcerpt' => $post->getPostExcerpt(),
                    ];
                    $category['items'][] = [
                        'image' => $image,
                        'post' => $postArr
                    ];
                }
            }
            $categories[$key] = $category;
        }
        return new JsonResponse(serialize($categories));
    }
}
