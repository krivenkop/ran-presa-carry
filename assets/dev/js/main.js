$(document).ready(function () {
	'use strict';

	svg4everybody();

	let blazy = new Blazy({
		loadInvisible: true,
		successClass: 'lazy--loaded',
		success: element => {
			if (element.tagName === 'IMG') element.parentNode.classList.add('lazy-container--loaded');
		}
	});

	function checkScrollbar() {
		var el = $('<div style="width:100px;height:100px;overflow:scroll;position:absolute;top:-9999px;"/>'),
			elDom = el.get(0);

		el.appendTo('body');

		if (elDom.offsetWidth !== elDom.clientWidth) {
			$('.page__wrapper').addClass('page__wrapper--has-scrollbar');
		}

		el.remove();
	}

	checkScrollbar();

	let body = $('body');

	function freezePage() {
		$('html, body').addClass('freezed');
	}

	function unfreezePage() {
		$('html, body').removeClass('freezed');
	}

	function toggleFreezePage() {
		$('html, body').toggleClass('freezed');
	}

	let heroSlider = $('.slider').owlCarousel({
		items: 1,
		lazyLoad:true,
		dots: false,
		nav: true,
		autoplay: true,
		smartSpeed: 400,
		autoplayHoverPause: true,
		navText: [
			'<svg class="svg-icon" role="img" title="Previous slide"><use xlink:href="img/icons/svg/sprite.symbol.svg#arrow-thin-left"></use></svg>',
			'<svg class="svg-icon" role="img" title="Next slide"><use xlink:href="img/icons/svg/sprite.symbol.svg#arrow-thin-right"></use></svg>']
	});
	heroSlider.trigger('refresh.owl.carousel');

	$('.js-nav-open').click(() => {
		$('.page-header__nav').addClass('page-header__nav--visible');
		freezePage();
	});

	$('.js-nav-close').click(() => {
		$('.page-header__nav').removeClass('page-header__nav--visible');
		unfreezePage();
	});

	var element = document.getElementById("calendar");
	jsCalendar.new(element, "30/01/2017", {
		language : "ru"
	});



	$('.hide-show__toggle').click(function () {
		let content = $(this).siblings('.hide-show__content')
			.get(0);

		if (content.style.maxHeight) {
			content.style.maxHeight = null;
		} else {
			content.style.maxHeight = content.scrollHeight + 'px';
		}
	});

	let headerSearchTimeout = null;

	$('.search__btn').on('click', function () {
		let search = $(this).closest('.search');

		search.find('.search__input')
			.toggleClass('search__input--active');

		if ($(this).hasClass('search__btn--active')) {
			search.removeClass('search--results-visible');

			search.find('.search__input')
				.blur();
		}

		

		$(this).toggleClass('search__btn--active');
	});

	$('.page-header__search-form').on('click', '.search__btn', function () {
		let pageHeader = $('.page-header');

		clearTimeout(headerSearchTimeout);

		if ($(this).hasClass('search__btn--active')) {
			pageHeader.addClass('page-header--search-opened');
		} else {
			headerSearchTimeout = setTimeout(function () {
				pageHeader.removeClass('page-header--search-opened')
			}, 800);
		}
	});

	let searchForm = (() => {
		let container = $('.search'),
			itemsContainer = container.find('.search__items'),
			timeout;

		let init = () => {
			container.on('keyup', () => {
				clearTimeout(timeout);

				timeout = setTimeout(sendAjax, 500);
			});
		};

		function sendAjax(data) {
			$.ajax({
				url: container.data('url'),
				type: 'GET',
				dataType: 'html',
				data: data,
				success: items => {
					container.addClass('search--results-visible');
					itemsContainer.html('')
						.html(items);
				}
			});
		}

		return {
			init: init
		}
	})();
	searchForm.init();
});